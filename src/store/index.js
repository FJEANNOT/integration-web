import Vue from 'vue'
import Vuex from 'vuex'

import transition from './transitionStore'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      transition
    },
    strict: process.env.DEBUGGING
  })

  return Store
}
